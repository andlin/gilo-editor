package main

import (
	"encoding/base64"
	"log"
	"math/rand"
	"os"
	"strings"
)

func editorSave(st *state, filename string) {
	st.filename = filename

	if len(strings.Trim(st.filename, " ")) == 0 {
		st.setStatusMessage("Save aborted")
		return
	}

	// Random data for temp file
	rbuf := make([]byte, 12)
	rand.Read(rbuf)
	rstring := base64.StdEncoding.EncodeToString(rbuf)

	contents := editorRowsToString(st)
	log.Printf("Contents len: %d\n", len(contents))
	file, err := os.OpenFile(st.filename+rstring, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		log.Panicf("save failed to open file: %v", err)
	}
	numbytes, err := file.WriteString(contents)
	if err != nil {
		log.Panicf("save failed to write file: %v", err)
	}
	err = file.Truncate(int64(len(contents)))
	if err != nil {
		log.Panicf("save failed to truncate file: %v", err)
	}
	err = file.Sync()
	if err != nil {
		log.Panicf("save failed to sync file: %v", err)
	}
	err = file.Close()
	if err != nil {
		log.Panicf("save failed to close file: %v", err)
	}
	err = os.Rename(st.filename+rstring, st.filename)
	if err != nil {
		log.Panicf("save failed link file: %v", err)
	}
	st.setStatusMessage("Save wrote %d bytes", numbytes)
	st.dirty = 0

	st.editorSelectSyntaxHighlight()
}

func editorRowsToString(st *state) string {
	scr := strings.Builder{}
	for i := 0; i < len(st.row); i++ {
		scr.WriteString(string(st.row[i].chars))
		if i != len(st.row)-1 {
			scr.WriteString("\n")
		}
	}
	return scr.String()
}
