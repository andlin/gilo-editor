package main

import (
	"bufio"
	"os"
)

// Terminal input escape codes
const (
	karrup = "[A"
	karrdn = "[B"
	karrl  = "[D"
	karrr  = "[C"
	khome  = "[H"
	khome2 = "OH"
	kend   = "[F"
	kend2  = "OF"
)

// keyInput is an input event from keyboard. It ca be one of:
// - a codepoint (rune)
// - an escape sequence (example: arrow movement)
// - or a modifier keypress (example: CTRL+Q)
type keyInput int32

// keyReader reads the next rune or key from input stream
// Relevant escape sequences are read and returned as "editor keys"
type keyReader struct {
	reader *bufio.Reader
}

func newKeyReader() keyReader {
	return keyReader{
		reader: bufio.NewReader(os.Stdin),
	}
}

func (reader *keyReader) next() keyInput {
	r, sz, err := reader.reader.ReadRune()
	if err == nil && sz > 0 {
		if r == besc { // Escape sequence - read more bytes
			if reader.reader.Buffered() >= 3 {
				b234, err := reader.reader.Peek(3)
				if err != nil {
					return keyInput(r)
				}
				_ = b234[0]
				b3 := b234[1]
				b4 := b234[2]
				if b3 >= '1' && b3 <= '9' {
					if b4 == '~' {
						switch b3 {
						case '1':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(home)
						case '3':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(del)
						case '4':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(end)
						case '5':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(pgUp)
						case '6':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(pgDn)
						case '7':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(home)
						case '8':
							_, err := reader.reader.Discard(3)
							if err != nil {
								return keyInput(r)
							}
							return keyInput(end)
						}
					}
				}
			}
			b23, err := reader.reader.Peek(2)
			if err != nil {
				return keyInput(r)
			}
			switch string(b23) {

			// Arrow movements
			case karrup:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(arrUp)
			case karrdn:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(arrDn)
			case karrl:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(arrL)
			case karrr:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(arrR)
			case khome:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(home)
			case khome2:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(home)
			case kend:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(end)
			case kend2:
				_, err := reader.reader.Discard(2)
				if err != nil {
					return keyInput(r)
				}
				return keyInput(end)
			}
		}
	}
	return keyInput(r)
}
