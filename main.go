package main

import (
	"bufio"
	"flag"
	"fmt"
	"golang.org/x/sys/unix"
	"io"
	"log"
	"os"
	"strings"
	"syscall"
	"time"
	"unicode"
	"unicode/utf8"
)

var CommitHash string
var Version string

const ioctlReadTermios = unix.TCGETS
const ioctlWriteTermios = unix.TCSETS

type editorSyntax struct {
	filetype               string
	filematch              []string
	keywords               []string
	singlelineCommentStart string
	multilineCommentStart  string
	multilineCommentStop   string
	flags                  uint
}

const (
	hlHighlightNumbers uint = 1 << iota
	hlHighlightStrings
)

var highlightdb = []editorSyntax{
	{
		"c",
		[]string{".c", ".h", ".cpp"},
		[]string{
			"switch", "if", "while", "for", "break", "continue", "return", "else",
			"struct", "union", "typedef", "static", "enum", "class", "case",
			"int|", "long|", "double|", "float|", "char|", "unsigned|", "signed|", "void|",
		},
		"//",
		"/*",
		"*/",
		hlHighlightNumbers | hlHighlightStrings,
	},
	{
		"go",
		[]string{".go"},
		[]string{
			"break", "default", "func", "interface", "select", "case", "defer", "go", "map", "struct",
			"chan", "else", "goto", "package", "switch", "const", "fallthrough", "if", "range", "type",
			"continue", "for", "import", "return", "var",
			"uint8|", "uint16|", "uint32|", "uint64|",
			"int8|", "int16|", "int32|", "int64|",
			"float32|", "float64|",
			"complex32|", "complex64|",
			"byte|",
			"rune|",
			"uint|",
			"int|",
			"uintptr|",
			"string|",
		},
		"//",
		"/*",
		"*/",
		hlHighlightNumbers | hlHighlightStrings,
	},
}

// row represents an editor row
type row struct {
	chars  []rune // Characters in document (file)
	render []rune // Characters that we render in editor
	hl     []int  // Highlighting

	// idx is this rows index
	idx           int
	hlOpenComment bool
}

func (r row) size() int {
	return len(r.chars)
}

func (s *state) updateRow(rownum int) {
	const tabStop = 8
	// Count tab occurences
	var tabs = 0
	if rownum < 0 || rownum >= len(s.row) {
		return
	}
	r := &s.row[rownum]
	for ch := range r.chars {
		if ch == '\t' {
			tabs++
		}
	}

	r.render = []rune{}
	var idx int
	for _, ch := range r.chars {
		if ch == '\t' {
			r.render = append(r.render, ' ')
			idx++
			for idx%tabStop != 0 {
				r.render = append(r.render, ' ')
				idx++
			}
		} else {
			r.render = append(r.render, rune(ch))
			idx++
		}
	}

	s.editorUpdateSyntax(rownum)
}

// Editor keys
const (
	bcksp = 127
	arrUp = utf8.MaxRune + iota
	arrDn
	arrL
	arrR
	pgUp
	pgDn
	home
	end
	del
)

// Editor highlights
const (
	hlNormal = iota
	hlString
	hlComment
	hlMLComment
	hlKeyword1
	hlKeyword2
	hlNumber
	hlMatchSearch
)

func RawMode(fd int) (*unix.Termios, error) {
	termios, err := unix.IoctlGetTermios(fd, ioctlReadTermios)
	if err != nil {
		return nil, err
	}

	state := *termios

	// This attempts to replicate the behaviour documented for cfmakeraw in
	// the termios(3) manpage.
	termios.Iflag &^= unix.IGNBRK | unix.BRKINT | unix.PARMRK | unix.ISTRIP | unix.INLCR | unix.IGNCR | unix.ICRNL | unix.IXON
	termios.Oflag &^= unix.OPOST
	termios.Lflag &^= unix.ECHO | unix.ECHONL | unix.ICANON | unix.ISIG | unix.IEXTEN
	termios.Cflag &^= unix.CSIZE | unix.PARENB
	termios.Cflag |= unix.CS8
	termios.Cc[unix.VMIN] = 1
	termios.Cc[unix.VTIME] = 0

	if err := unix.IoctlSetTermios(fd, ioctlWriteTermios, termios); err != nil {
		fmt.Printf("Failed entering raw mode\n")
		return nil, err
	}
	return &state, nil
}

func RestoreMode(fd int, termios unix.Termios) error {
	return unix.IoctlSetTermios(fd, ioctlWriteTermios, &termios)
}

// Bitwise AND with value 0001 1111 (1F). CTRL key modifies the upper 3 bits to 0.
func controlKey(b byte) keyInput {
	return keyInput(b & 0x1f)
}

func (s *state) processKeypress(d keyInput) {
	switch s.mode {
	case PROMPT:
		switch d {
		case del:
			fallthrough
		case controlKey('h'):
			fallthrough
		case bcksp:
			to := len(s.buf) - 1
			if to < 0 {
				to = 0
			}
			s.buf = s.buf[:to]
			s.promptTextUpdate(s.prompt, string(s.buf))
		case besc:
			s.buf = s.buf[0:0]
			s.promptTextClear()
			s.mode = STD
		case bcr:
			s.mode = STD
			s.promptCallback(string(s.buf))
			s.buf = s.buf[0:0]
		default:
			if unicode.IsGraphic(rune(d)) {
				s.buf = append(s.buf, rune(d))
				s.promptTextUpdate(s.prompt, string(s.buf))
			}
		}
		s.refreshScreen()
	case SEARCH:
		switch d {
		case del:
			fallthrough
		case controlKey('h'):
			fallthrough
		case bcksp:
			to := len(s.buf) - 1
			if to < 0 {
				to = 0
			}
			s.buf = s.buf[:to]
		case controlKey('q'):
			fallthrough
		case besc:
			s.setStatusMessage("")
			s.refreshScreen()
			s.mode = STD
			return
		case bcr:
			s.setStatusMessage("")
			s.refreshScreen()
			s.mode = STD
			return
		}
		if unicode.IsGraphic(rune(d)) {
			s.buf = append(s.buf, rune(d))
		}
		s.setStatusMessage("Search: %s (use ESC/Arrows/Enter)", string(s.buf))
		s.refreshScreen()
		s.searchCallback(s.buf, d)
	case STD:
		switch d {
		case controlKey('q'):

			// Unmodified (clean) file quits right away
			if s.dirty == 0 {
				close(s.stop)
			}

			// Modified (dirty) file, require three attempts
			s.quittimes++
			if s.quittimes >= 3 {
				close(s.stop)
			}
			return
		case arrDn:
			fallthrough
		case arrUp:
			fallthrough
		case arrL:
			fallthrough
		case arrR:
			fallthrough
		case pgDn:
			fallthrough
		case pgUp:
			fallthrough
		case home:
			fallthrough
		case end:
			s.moveCursor(d)

		case bcr:
			s.editorInsertNewline()
			break

		case controlKey('d'):
			fallthrough
		case del:
			s.moveCursor(arrR)
			fallthrough
		case bcksp:
			fallthrough
		case controlKey('h'):
			editorDelChar(s)

		case controlKey('l'):
			fallthrough
		case '\x1b':
			break

		case controlKey('f'):
			s.mode = SEARCH
			s.promptTextUpdate("Search: %s (use ESC/Arrows/Enter)", "")
			s.searchCallback = s.editorFindCallback()

		case controlKey('s'):
			if len(strings.Trim(s.filename, " ")) == 0 {
				// New file: Prompt for filename and write
				s.mode = PROMPT
				s.promptTextUpdate("Save as: %s", "")
				s.promptCallback = func(input string) {
					editorSave(s, input)
				}
			} else {
				// Existing file: just write
				editorSave(s, s.filename)
			}

		default:
			editorInsertChar(s, rune(d))
		}
		s.quittimes = 0
		s.refreshScreen()
	}
}

type position struct {
	x int
	y int
}

type inputMode int

const (
	STD inputMode = iota
	SEARCH
	PROMPT
)

func (i inputMode) String() string {
	return [...]string{"STD", "SEARCH", "PROMPT"}[i]
}

type state struct {
	pos       position // Cursor position
	rx        int      // Render X position
	rowOffset int      // what row of the file the user is currently scrolled to
	colOffset int      // column offset for horizontal scrolling

	// Screen size
	nrows int
	ncols int

	termios       *unix.Termios
	filename      string
	syntax        editorSyntax
	debugMsg      string    // Debug message in status bar
	statusMsg     string    // Status message in status bar
	statusMsgTime time.Time // Status message time stamp

	row       []row
	dirty     int       // Dirty if file content is modified
	stop      chan bool // Stop channel
	quittimes int       // Require multiple Ctrl-Q to quit

	keyReader      keyReader
	searchCallback searchCallback
	buf            []rune         // Prompt input buffer
	mode           inputMode      // How keyboard input are processed
	promptCallback promptCallback // Handles prompt result
	prompt         string
}

func (s *state) promptTextClear() {
	s.prompt = ""
	s.setStatusMessage("")
}

func (s *state) promptTextUpdate(prompt string, args ...interface{}) {
	s.prompt = prompt
	s.setStatusMessage(s.prompt, args)
}

func (s *state) setStatusMessage(format string, args ...interface{}) {
	s.statusMsg = fmt.Sprintf(format, args...)
	if len(s.statusMsg) > 80 {
		s.statusMsg = s.statusMsg[0:80]
	}
	s.statusMsgTime = time.Now()
}

func (s *state) setDebug(format string, args ...interface{}) {
	s.debugMsg = fmt.Sprintf(format, args...)
	if len(s.debugMsg) > 60 {
		s.debugMsg = s.debugMsg[:60]
	}
}

func (s *state) editorInsertNewline() {
	if s.pos.x == 0 {
		s.editorInsertRow(s.pos.y, "")
	} else {
		row := &s.row[s.pos.y]
		s.editorInsertRow(s.pos.y+1, string(row.chars[s.pos.x:]))
		row = &s.row[s.pos.y]
		row.chars = row.chars[:s.pos.x]
		s.updateRow(s.pos.y)
	}
	s.pos.y++
	s.pos.x = 0
}

func (s *state) moveup(n int) {
	s.pos.y -= n
	if s.pos.y < 0 {
		s.pos.y = 0
	}
}

func (s *state) movedn(n int) {
	s.pos.y += n
	if s.pos.y > len(s.row) {
		s.pos.y = len(s.row)
	}
}

func (s *state) movel() {
	if s.pos.x != 0 {
		s.pos.x--
	} else if s.pos.y > 0 {
		s.pos.y--
		s.pos.x = len(s.row[s.pos.y].chars)
	}
	if s.pos.x < 0 {
		s.pos.x = 0
	}
}

func (s *state) mover() {
	var row []rune = nil
	if s.pos.y < len(s.row) {
		row = s.row[s.pos.y].chars
	}
	if row != nil && s.pos.x < len(row) {
		s.pos.x++
	} else if row != nil && s.pos.x == len(row) {
		s.pos.y++
		s.pos.x = 0
	}
}

func (s *state) pgup() {
	s.pos.y = s.rowOffset
	s.moveup(s.nrows)
}

func (s *state) pgdn() {
	s.pos.y = s.rowOffset + s.nrows - 1
	if s.pos.y > len(s.row) {
		s.pos.y = len(s.row)
	}
	s.movedn(s.nrows)
}

func (s *state) home() {
	s.pos.x = 0
}

func (s *state) end() {
	if s.pos.y < len(s.row) {
		s.pos.x = s.row[s.pos.y].size()
	}
}

//moveCursor updates cursor position on keypress
func (s *state) moveCursor(b keyInput) {
	switch b {
	case arrUp:
		s.moveup(1)
	case arrL:
		s.movel()
	case arrDn:
		s.movedn(1)
	case arrR:
		s.mover()
	case pgUp:
		s.pgup()
	case pgDn:
		s.pgdn()
	case home:
		s.home()
	case end:
		s.end()
	}

	var row []rune = nil
	if s.pos.y < len(s.row) {
		row = s.row[s.pos.y].chars
	}
	var rowlen int
	if row == nil {
		rowlen = 0
	} else {
		rowlen = len(row)
	}
	if s.pos.x > rowlen {
		s.pos.x = rowlen
	}
}

// OUTPUT //
const (
	bcr       = '\r'          // Carriage return
	besc      = '\x1b'        // ESC character
	esc       = "\x1b"        // ESC character
	cls       = esc + "[2J"   // Clear screen
	clearline = esc + "[K"    // Clear line
	curhome   = esc + "[H"    // Cursor home
	curhide   = esc + "[?25l" // Cursor hide
	curshow   = esc + "[?25h" // Cursor show
)

func drawRows(st *state, scr *strings.Builder) {
	numrows := len(st.row)
	for nr := 0; nr < st.nrows; nr++ {
		fileRow := nr + st.rowOffset
		if fileRow >= numrows {
			if numrows == 0 && nr == (st.nrows/3) {
				// Draw welcome message
				msg := fmt.Sprintf("Gilo editor -- version v%s (%s)", Version, CommitHash)
				lpadding := ((st.ncols - len(msg)) / 2) - 1
				scr.WriteString("~")
				scr.WriteString(strings.Repeat(" ", lpadding))
				scr.WriteString(msg)
			} else {
				scr.WriteString("~")
			}
		} else {
			if fileRow < numrows {
				colOffset := st.colOffset
				line := st.row[fileRow].render
				hl := st.row[fileRow].hl

				// Debug: Mark current line
				//if fileRow == s.pos.y {
				//	line = append(line, " *"...)
				//}

				l := len(line) - colOffset
				if l < 0 {
					l = 0
				}
				if l > st.ncols {
					l = st.ncols
				}
				start := colOffset
				if start > len(line) {
					start = 0
				}
				stop := colOffset + l
				if stop > len(line) {
					stop = len(line)
				}
				if stop > st.ncols {
					stop = st.ncols
				}
				currentColor := -1
				for idx, c := range line[start:stop] {
					if unicode.IsControl(c) {
						var charSym []rune
						if c <= 26 {
							charSym = append(charSym, '@', c)
						} else {
							charSym = append(charSym, '?')
						}
						scr.WriteString("\x1b[7m")
						scr.WriteString(string(charSym))
						scr.WriteString("\x1b[m")
						if currentColor != -1 {
							scr.WriteString(fmt.Sprintf("\x1b[%dm", currentColor))
						}
					} else if hl[idx] == hlNormal {
						if currentColor != -1 {
							scr.WriteString("\x1b[39m")
							currentColor = -1
						}
						scr.WriteRune(c)
					} else {
						clr := editorSyntaxToColor(hl[idx])
						if clr != currentColor {
							currentColor = clr
							scr.WriteString(fmt.Sprintf("\x1b[%dm", clr))
						}
						scr.WriteRune(c)
					}
				}
				scr.WriteString("\x1b[39m")
			}
		}
		scr.WriteString(clearline)
		scr.WriteString("\r\n")
	}
}

func isSeparator(b rune) bool {
	return unicode.IsPunct(b) || unicode.IsSymbol(b) || unicode.IsSpace(b)
}

func editorRowCxToRx(r row, cx int) int {
	const tabStop = 8
	var rx int
	for j := 0; j < cx; j++ {
		if r.chars[j] == '\t' {
			rx += (tabStop - 1) - (rx % tabStop)
		}
		rx++
	}
	return rx
}

func editorRowRxToCx(r row, rx int) int {
	const tabStop = 8
	var currx, cx int
	for cx = range r.chars {
		if r.chars[cx] == '\t' {
			currx += (tabStop - 1) - (currx % tabStop)
		}
		currx++
		if currx > rx {
			return cx
		}
	}
	return cx
}

// refreshScreen redraws the screen
func (s *state) refreshScreen() {
	var scr strings.Builder

	editorScroll(s)
	scr.WriteString(curhide)
	scr.WriteString(curhome)
	drawRows(s, &scr)
	drawStatusBar(s, &scr)
	drawMessageBar(s, &scr)
	placeCursor(s, &scr)
	scr.WriteString(curshow)
	_, _ = io.WriteString(os.Stdout, scr.String())
}

func drawStatusBar(s *state, scr *strings.Builder) {
	scr.WriteString(esc + "[7m")
	filename := s.filename
	if filename == "" {
		filename = "Unnamed"
	}
	if s.dirty > 0 {
		filename = fmt.Sprintf("%s (%s)", filename, "modified")
	}
	smsg := fmt.Sprintf("%.20s - %d lines [%.20s]", filename, len(s.row), s.debugMsg)

	rmsg := fmt.Sprintf("%s | %d/%d", s.syntax.filetype, s.pos.y+1, len(s.row))

	scr.WriteString(smsg)
	scr.WriteString(strings.Repeat(" ", s.ncols-len(smsg)-len(rmsg)))
	scr.WriteString(rmsg)
	scr.WriteString(esc + "[m")
	scr.WriteString("\r\n")
}

func drawMessageBar(st *state, scr *strings.Builder) {
	scr.WriteString(esc + "[K")
	if st.statusMsgTime.Add(time.Second * 5).After(time.Now()) {
		msgLen := len(st.statusMsg)
		if msgLen > st.ncols {
			msgLen = st.ncols
		}
		scr.WriteString(st.statusMsg[0:msgLen])
	}
}

// placeCursor places cursor on the correcy x/y-position on screen
func placeCursor(s *state, scr *strings.Builder) {
	y := (s.pos.y - s.rowOffset) + 1
	x := (s.rx - s.colOffset) + 1
	movecur := fmt.Sprintf("%s[%d;%dH", esc, y, x)
	scr.WriteString(movecur)
}

// getWindowSize finds out the windows size
func getWindowSize() (int, int) {
	ws, err := unix.IoctlGetWinsize(syscall.Stdout, unix.TIOCGWINSZ)
	if err != nil {
		log.Fatalf("getWindowSize: fail: %v\n. see: https://viewsourcecode.org/snaptoken/kilo/03.rawInputAndOutput.html#window-size-the-hard-way", err)
	}
	return int(ws.Row), int(ws.Col)
}

func (s *state) editorUpdateSyntax(rownum int) {
	r := &s.row[rownum]
	m := len(r.render)
	if cap(r.render) > cap(r.hl) {
		newSlice := make([]int, (m+1)*2)
		copy(newSlice, r.hl)
		r.hl = newSlice
	}

	keywords := s.syntax.keywords

	scs := s.syntax.singlelineCommentStart
	mcstart := s.syntax.multilineCommentStart
	mcstop := s.syntax.multilineCommentStop

	prevSep := 1
	inString := rune(0)
	inComment := r.idx > 0 && s.row[r.idx-1].hlOpenComment

	i := 0
	for i < len(r.render) {
		c := r.render[i]
		r.hl[i] = hlNormal
		prevHl := hlNormal
		if i != 0 {
			prevHl = r.hl[i-1]
		}

		if len(scs) > 0 && inString == rune(0) && !inComment {
			if i+len(scs) <= len(r.render) && string(r.render[i:i+len(scs)]) == scs {
				for j := range r.render { // Highlight to the end of line
					r.hl[j] = hlComment
				}
				break
			}
		}

		if len(mcstart) > 0 && len(mcstop) > 0 && inString == rune(0) {
			if inComment {
				r.hl[i] = hlMLComment
				if len(r.render) >= i+len(mcstop) && string(r.render[i:i+len(mcstop)]) == mcstop {
					for mi := range mcstop {
						r.hl[i+mi] = hlMLComment
					}
					i += len(mcstop)
					inComment = false
					prevSep = 1
					continue
				} else {
					i++
					continue
				}
			} else if len(r.render) >= i+len(mcstart) && string(r.render[i:i+len(mcstart)]) == mcstart {
				for mi := range mcstart {
					r.hl[i+mi] = hlMLComment
				}
				i += len(mcstart)
				inComment = true
				continue
			}
		}

		if s.syntax.flags&hlHighlightStrings != 0 {
			if inString != rune(0) {
				r.hl[i] = hlString
				if c == '\\' && i+1 < len(r.render) { // Handle escaped characters in strings
					r.hl[i+1] = hlString
					i += 2
					continue
				}
				if c == inString {
					inString = rune(0)
				}
				prevSep = 1
				i++
				continue
			} else {
				if c == '"' || c == '\'' || c == '`' {
					inString = c
					r.hl[i] = hlString
					i++
					continue
				}
			}
		}
		if s.syntax.flags&hlHighlightNumbers != 0 {
			if unicode.IsNumber(c) && (prevSep != 0 || prevHl == hlNumber) ||
				(c == ',' && prevHl == hlNumber) ||
				(c == '.' && prevHl == hlNumber) {
				r.hl[i] = hlNumber
				i++
				prevSep = 0
				continue
			}
		}

		// Highlight keywords
		if prevSep != 0 {
			var k int
			for k = range keywords {
				kw := keywords[k]
				kwLen := len(kw)
				kw2 := false
				if kw[len(kw)-1] == byte('|') {
					kw2 = true
					kw = kw[0 : len(kw)-1]
					kwLen -= 1
				}
				if i+kwLen <= len(r.render) &&
					string(r.render[i:i+kwLen]) == kw &&
					(len(r.render) == i+kwLen || isSeparator(r.render[i+kwLen])) {
					for l := 0; l <= kwLen; l++ {
						if !kw2 {
							r.hl[i+l] = hlKeyword2
						} else {
							r.hl[i+l] = hlKeyword1
						}
					}
					i += kwLen
					break
				}
			}
			if k < len(keywords) {
				// If we broke out of keywords for-loop, continue looking at next letter
				prevSep = 0
				continue
			}
		}

		if isSeparator(c) {
			prevSep = 1
		} else {
			prevSep = 0
		}
		i++
	}
	changed := r.hlOpenComment != inComment
	r.hlOpenComment = inComment
	if changed && r.idx+1 < len(s.row) {
		s.editorUpdateSyntax(r.idx + 1)
	}
}

func (s *state) editorSelectSyntaxHighlight() {
	s.syntax = editorSyntax{}
	if s.filename == "" {
		return
	}

	var ext string
	dix := strings.LastIndex(s.filename, ".")
	if dix > 0 {
		ext = s.filename[dix:]
	}

	for _, dbe := range highlightdb {
		isExt := false
		for _, fm := range dbe.filematch {
			if fm[0] == '.' {
				isExt = true
			}
			if (isExt && ext == fm) ||
				(!isExt && s.filename == fm) {
				s.syntax = dbe

				// Highlight entire file
				for i := 0; i < len(s.row); i++ {
					s.editorUpdateSyntax(i)
				}
				return
			}
		}
	}
}

func editorSyntaxToColor(hl int) int {
	switch hl {
	case hlNumber:
		return 31
	case hlMatchSearch:
		return 34
	case hlString:
		return 35
	case hlMLComment:
		return 36
	case hlComment:
		return 36
	case hlKeyword1:
		return 33
	case hlKeyword2:
		return 32
	default:
		return 37
	}
}

func editorInsertChar(s *state, r rune) {
	if r >= unicode.MaxRune {
		return
	}
	if s.pos.y == len(s.row) {
		s.editorInsertRow(len(s.row), "")
	}
	s.editorRowInsertChar(s.pos.x, r)
	s.pos.x++
	s.dirty++
}

func (s *state) editorRowInsertChar(at int, r rune) {
	row := &s.row[s.pos.y]
	if at < 0 || at > len(row.chars) {
		at = len(row.chars)
	}
	nbuf := append([]rune{}, row.chars[0:at]...)
	nbuf = append(nbuf, r)
	row.chars = append(nbuf, row.chars[at:]...)
	s.updateRow(s.pos.y)
}

func editorDelChar(s *state) {
	if s.pos.y == len(s.row) {
		return
	}
	if s.pos.y == 0 && s.pos.x == 0 {
		return
	}

	row := &s.row[s.pos.y]
	if s.pos.x > 0 {
		s.editorRowDelChar(row, s.pos.x-1)
		s.pos.x--
	} else {
		s.pos.x = len(s.row[s.pos.y-1].chars)
		s.editorRowAppendString(row.chars, s.pos.y-1)
		editorDelRow(s, s.pos.y)
		s.pos.y--
	}
	s.updateRow(s.pos.y)
}

func (s *state) editorRowDelChar(row *row, at int) {
	if at < 0 || at > len(row.chars) {
		return
	}
	row.chars = append(row.chars[:at], row.chars[at+1:]...)
	s.updateRow(at)
}

func (s *state) editorInsertRow(at int, line string) {
	if at < 0 || at > len(s.row) {
		return
	}

	newrow := row{[]rune(line), nil, nil, at, false}
	s.row = append(s.row, newrow)
	copy(s.row[at+1:], s.row[at:])
	s.row[at] = newrow
	s.updateRow(at)
	s.dirty++

	for j := at + 1; j < len(s.row); j++ {
		s.row[j].idx++
	}
}

func editorDelRow(s *state, at int) {
	if s.pos.y == len(s.row) {
		return
	}
	s.row = append(s.row[:at], s.row[at+1:]...)

	for j := at; j <= len(s.row)-1; j++ {
		s.row[j].idx--
	}

	s.dirty++
}

func (s *state) editorRowAppendString(str []rune, at int) {
	row := &s.row[at]
	row.chars = append(row.chars, str...)
	s.updateRow(at)
	s.dirty++
}

func editorScroll(s *state) {
	if s.pos.y < len(s.row) {
		s.rx = editorRowCxToRx(s.row[s.pos.y], s.pos.x)
	}

	if s.pos.y < s.rowOffset {
		s.rowOffset = s.pos.y
	}
	if s.pos.y >= s.rowOffset+(s.nrows) {
		s.rowOffset = (s.pos.y - s.nrows) + 1
	}
	if s.rx < s.colOffset {
		s.colOffset = s.rx
	}
	if s.rx >= s.colOffset+(s.ncols) {
		s.colOffset = s.rx - s.ncols + 1
	}
}

type searchCallback = func([]rune, keyInput)
type promptCallback = func(input string)

// initialize initializes our state struct
func initialize(st *state) {
	st.nrows, st.ncols = getWindowSize() // Query window size
	st.nrows--                           // Make room for status bar
	st.nrows--                           // Make room for message bar

	st.stop = make(chan bool) // Stop channel signals quit
	st.keyReader = newKeyReader()
}

func (s *state) editorFindCallback() searchCallback {
	lastMatch, direction := -1, 1
	var savedHlLine int
	var savedHl []int
	return func(query []rune, key keyInput) {
		if savedHl != nil {
			copy(s.row[savedHlLine].hl, savedHl)
			savedHl = nil
		}
		if key == bcr || key == besc {
			lastMatch = -1
			direction = 1
			return
		} else if key == arrR || key == arrDn {
			direction = 1
		} else if key == arrL || key == arrUp {
			direction = -1
		} else {
			lastMatch = -1
			direction = 1
		}

		current := lastMatch
		for range s.row {
			current += direction
			if current <= -1 {
				current = len(s.row) - 1
			} else if current == len(s.row) {
				current = 0
			}
			erow := s.row[current]
			match := strings.Index(string(erow.render), string(query))
			if match >= 0 {
				lastMatch = current
				s.pos.y = current
				s.pos.x = editorRowRxToCx(erow, match)
				s.rowOffset = len(s.row)

				savedHlLine = current
				savedHl = make([]int, len(erow.hl))
				copy(savedHl, erow.hl)
				for i := 0; i < len(query); i++ {
					idx := match + i
					erow.hl[idx] = hlMatchSearch
				}
				break
			}
		}
	}
}

func editorOpen(s *state, filename string) {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	r := bufio.NewReader(f)
	for {
		str, err := r.ReadString('\n')
		if err != nil {
			if err == io.EOF && len(str) > 0 {
				str = str[0 : len(str)-1] // String '\n'
				s.editorInsertRow(len(s.row), str)
			}
			break
		}
		str = str[0 : len(str)-1] // String '\n'
		s.editorInsertRow(len(s.row), str)
	}
	s.filename = filename
	s.dirty = 0

	s.editorSelectSyntaxHighlight()
}

func main() {
	// Read configuration
	state := &state{}
	initialize(state)

	// Parse command arguments
	boolFlag := flag.Bool("version", false, "Prints version")
	flag.Parse()

	if *boolFlag {
		fmt.Printf("gilo editor v%s\n", Version)
		return
	}

	// Terminal modes
	var err error
	state.termios, err = RawMode(syscall.Stdin)
	defer func() {
		_, _ = io.WriteString(os.Stdout, cls+curhome) // On exit: Clear screen and park cursor
		_ = RestoreMode(syscall.Stdin, *state.termios)
	}()
	if err != nil {
		panic(err)
	}

	// Open file if provided as the last argument
	if len(flag.Args()) >= 1 {
		filename := flag.Arg(0)
		editorOpen(state, filename)
	}

	state.setStatusMessage("HELP: Ctrl-S = save | Ctrl-Q = quit | Ctrl-F = find")

	ch := make(chan keyInput, 1024)
	go func() {
		for {
			ch <- state.keyReader.next()
		}
	}()
	for {
		state.refreshScreen()
		select {
		case keyEvent := <-ch:
			state.processKeypress(keyEvent)
		case <-state.stop:
			return // Stop signal - exit gilo
		}
	}
}
