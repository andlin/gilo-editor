package main

import (
	"bufio"
	"testing"
)

type mockReader struct {
	toread []int32
}

func (m mockReader) Read(p []byte) (n int, err error) {
	copied := copy(p, string(m.toread))
	return copied, nil
}

func answerString(r string) *bufio.Reader {
	mr := mockReader{
		toread: []int32(r),
	}
	return bufio.NewReader(mr)
}

func answerKeyInput(b keyInput) *bufio.Reader {
	mr := mockReader{
		toread: []int32{int32(b)},
	}
	return bufio.NewReader(mr)
}

func TestReadsChar(t *testing.T) {
	reader := answerString("a")
	kr := keyReader{reader: reader}
	r := kr.next()
	if r != 'a' {
		t.Errorf("Read: %c, expected %c", r, 'a')
	}
}

func TestReadsNumber(t *testing.T) {
	reader := answerString("8")
	kr := keyReader{reader: reader}
	r := kr.next()
	if r != '8' {
		t.Errorf("Read: %c, expected %c", r, '8')
	}
}

func TestReadsControlSUpper(t *testing.T) {
	reader := answerKeyInput(controlKey('S'))
	kr := keyReader{reader: reader}
	r := kr.next()
	if r != 0x13 {
		t.Errorf("Read: %d, expected %d", r, 0x13)
	}
}

func TestReadsControlSLower(t *testing.T) {
	reader := answerKeyInput(controlKey('s'))
	kr := keyReader{reader: reader}
	r := kr.next()
	if r != 0x13 {
		t.Errorf("Read: %d, expected %d", r, 0x13)
	}
}

func escape(str string) string {
	const ESC = byte(0x1B)
	return string(ESC) + str
}

func TestReadsHome(t *testing.T) {
	reader := answerString(escape("[1~"))
	kr := keyReader{reader: reader}
	r := kr.next()
	if r != home {
		t.Errorf("Read: %d, expected %d", r, home)
	}
}

func TestReadsArrowUp(t *testing.T) {
	reader := answerString(escape("[A"))
	kr := keyReader{reader: reader}
	r := kr.next()
	if r != arrUp {
		t.Errorf("Read: %d, expected %d", r, arrUp)
	}
}
