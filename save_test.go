package main

import (
	"bytes"
	"testing"
)

func TestOneLine(t *testing.T) {
	st := &state{
		row: []row{
			{
				chars: []rune("abc"),
				idx:   0,
			},
		},
	}
	toString := editorRowsToString(st)
	t.Log([]byte(toString))
	t.Log([]byte("abc"))
	if !bytes.Equal([]byte(toString), []byte("abc")) {
		t.Fail()
	}
}

func TestTwoLines(t *testing.T) {
	st := &state{
		row: []row{
			{
				chars: []rune("abc"),
			},
			{
				chars: []rune("def"),
			},
		},
	}
	toString := editorRowsToString(st)
	t.Log([]byte(toString))
	t.Log([]byte("abc"))
	if !bytes.Equal([]byte(toString), []byte("abc\ndef")) {
		t.Fail()
	}
}

func TestMultibyteRunes(t *testing.T) {
	st := &state{
		row: []row{
			{
				chars: []rune("æøå"),
			},
			{
				chars: []rune("😅"),
			},
		},
	}
	toString := editorRowsToString(st)
	t.Log([]byte(toString))
	t.Log([]byte("abc"))
	if !bytes.Equal([]byte(toString), []byte("æøå\n😅")) {
		t.Fail()
	}
}
