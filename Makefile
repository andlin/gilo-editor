COMMITHASH=$$(git rev-parse --short HEAD)
VERSION=$$(cat VERSION)
LDFLAGS=-ldflags "-X main.CommitHash=$(COMMITHASH) -X main.Version=$(VERSION)"
GOFILES := $(wildcard *.go)
GOMODFILES :=$(wildcard go.*)

all: fmt test build

gilo : $(GOFILES) $(GOMODFILES) VERSION
	go build \
	$(LDFLAGS)

build : gilo

install :
	go install -a \
	$(LDFLAGS)

fmt :
	go fmt

test:
	go test \
	$(LDFLAGS)

.PHONY: build fmt install test
